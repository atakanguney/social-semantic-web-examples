import React from 'react';
import './App.css';
import annotator from 'annotator';

function App() {
  var app = new annotator.App();
  app.include(annotator.ui.main, {element: document.body});
  app.start();
  return (
      <div className="App">
        <header className="App-header">
          <h1>
            My Awesome Annotative Web Site
          </h1>
        </header>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit sapien ligula, vel feugiat est tincidunt a. Cras a sodales felis. Integer eleifend tempor urna at accumsan. Donec condimentum iaculis nunc ut dapibus. Integer sed arcu dolor. Nullam ullamcorper neque urna, eu vulputate nibh fermentum et. Mauris ac dignissim lectus. Sed facilisis tellus vel accumsan commodo. Donec facilisis ipsum tellus.
        </p>
        <p>
          Nam lobortis eleifend metus, facilisis auctor nulla semper id. Fusce vitae facilisis nisl, eu porta purus. Morbi scelerisque, purus sit amet placerat varius, est nisl viverra quam, quis vehicula purus neque vestibulum nulla. Fusce eleifend finibus scelerisque. Duis sagittis non diam vitae pretium. Proin malesuada finibus vehicula. Suspendisse tempor mauris ut libero tincidunt suscipit.
        </p>
        <p>
          Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam et felis magna. Sed laoreet nunc sit amet velit convallis euismod sit amet in tellus. Donec viverra egestas porta. Sed lacinia enim ac pulvinar tristique. Ut eget imperdiet nibh. Nam ipsum magna, imperdiet id urna et, lobortis porttitor lectus. Integer venenatis dolor odio, nec aliquam tellus consectetur eget.
        </p>
        <p>
          In ac posuere ex. Nunc iaculis lectus blandit ante mattis euismod. Duis non mattis sem. Proin nec elit in nunc varius posuere id quis dui. Sed at lacus congue lectus volutpat ultrices sed at nisl. Etiam sed mi hendrerit, ultrices nibh et, pellentesque massa. Pellentesque et felis dictum, dapibus mauris et, varius nulla. Nulla condimentum rutrum egestas. Donec ac gravida arcu. Suspendisse in malesuada felis, ut iaculis arcu. Nunc euismod metus nec ex tristique gravida. Quisque accumsan accumsan viverra.
        </p>
        <p>
          Suspendisse suscipit, ligula eu lobortis malesuada, enim erat fermentum tellus, non placerat justo lorem ut arcu. Maecenas massa erat, cursus vel laoreet nec, gravida et metus. Fusce tempus nunc massa, sit amet egestas orci tempus non. Nulla tempor metus ac dapibus congue. Mauris ornare, augue ac consequat suscipit, nulla est lobortis urna, cursus molestie lorem nisi quis purus. Curabitur at fermentum sapien. Fusce convallis accumsan enim, sed rhoncus ante fringilla eu. Curabitur egestas vitae ex eu ultricies. Aenean eu blandit ex. Quisque id scelerisque nulla. Sed rutrum libero efficitur pretium feugiat. Integer nec lacus tortor. Cras auctor enim a condimentum ultricies. 
        </p>
      </div>
  );
}

export default App;
