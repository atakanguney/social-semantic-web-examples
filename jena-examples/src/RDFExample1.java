import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.*;

import java.io.FileWriter;
import java.io.IOException;


public class RDFExample1 extends Object {
    public static void main (String args[]) throws IOException {
        // create an empty model
        Model model = ModelFactory.createDefaultModel();

        // create the resource
        //   and add the properties cascading style
        Resource greenGoblin
                = model.createResource()
                .addProperty(RDF.type, FOAF.Person)
                .addProperty(FOAF.name, "Green Goblin");

        Resource spiderman
                = model.createResource()
                .addProperty(RDF.type, FOAF.Person)
                .addProperty(FOAF.name, "Spiderman");

        greenGoblin.addProperty(Relationship.enemyOf, spiderman);
        spiderman.addProperty(Relationship.enemyOf, greenGoblin);

        // list the statements in the graph
        StmtIterator iter = model.listStatements();

        // print out the predicate, subject and object of each statement
        while (iter.hasNext()) {
            Statement stmt      = iter.nextStatement();         // get next statement
            Resource  subject   = stmt.getSubject();   // get the subject
            Property  predicate = stmt.getPredicate(); // get the predicate
            RDFNode   object    = stmt.getObject();    // get the object

            System.out.print(subject.toString());
            System.out.print(" " + predicate.toString() + " ");
            if (object instanceof Resource) {
                System.out.print(object.toString());
            } else {
                // object is a literal
                System.out.print(" \"" + object.toString() + "\"");
            }
            System.out.println(" .");
        }

        FileWriter fileWriter = new FileWriter("results/example-1.ttl");
        model.write(fileWriter, "TURTLE");
    }
}