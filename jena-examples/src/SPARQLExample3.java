import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.util.FileManager;

public class SPARQLExample3 {
    public static void main(String[] args) {
        FileManager.get().addLocatorClassLoader(SPARQLExample3.class.getClassLoader());
        Model model = FileManager.get().loadModel("data/data-1.ttl");

        String queryString = "PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
                "PREFIX : <http://example.org/> " +
                "SELECT ?name WHERE { " +
                "   ?person foaf:knows :suzan ." +
                "   ?person foaf:name ?name ." +
                "}";

        Query query = QueryFactory.create(queryString);
        QueryExecution qexec = QueryExecutionFactory.create(query, model);
        try{
            ResultSet results = qexec.execSelect();
            while (results.hasNext()){
                QuerySolution soln = results.nextSolution();
                Literal name = soln.getLiteral("name");
                System.out.println(name);
            }
        } finally {
            qexec.close();
        }
    }
}
