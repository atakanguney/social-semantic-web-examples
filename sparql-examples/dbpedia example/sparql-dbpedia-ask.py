from SPARQLWrapper import SPARQLWrapper, XML

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setQuery("""
    ASK WHERE { 
        <http://dbpedia.org/resource/Turkey> rdfs:label "Türkei"@de
    }    
""")

sparql.setReturnFormat(XML)
results = sparql.query().convert()
print(results.toxml())
