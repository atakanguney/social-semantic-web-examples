
import rdflib
import rdfextras
rdfextras.registerplugins()

file_path = "sample.ttl"
g = rdflib.Graph()
g.parse(file_path, format="turtle")

results = g.query("""
SELECT ?name
WHERE{
?x foaf:name ?name
}
""")

for r in results:
    print(r[0])
